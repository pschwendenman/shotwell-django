from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.entire_library, name='library'),
    url(r'^tag/$', views.list_tags, name='tags'),
    url(r'^tag/(?P<tag_name>[-A-Za-z()\' #?/]+?)/$', views.tag, name='tag'),
    url(r'^rating/(?P<rating>-?[0-5])/$', views.rating, name='rating'),
]
