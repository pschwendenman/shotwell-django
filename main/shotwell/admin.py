from django.contrib import admin

from .models import BackingPhoto, Event, Photo, Tag, Tombstone, Version, Video

class BackingPhotoAdmin(admin.ModelAdmin):
        list_display = ('id', 'filepath', 'timestamp', 'filesize', 'width',
                        'height', 'original_orientation', 'file_format',
                        'time_created')

class EventAdmin(admin.ModelAdmin):
    list_display = ('name', 'time_created', 'comment')

class PhotoAdmin(admin.ModelAdmin):
    list_display = ('title', 'rating', 'image_name', 'directory_name', 'height', 'width', 'md5', 'tags')
    list_filter = ('rating',)
    search_fields = ('md5', 'filename')


class TagAdmin(admin.ModelAdmin):
    list_display = ('name', 'time_created', 'number_of_images', 'photo_id_list')


class TombstoneAdmin(admin.ModelAdmin):
    list_display = ('id', 'filepath', 'filesize', 'time_created', 'reason',
                    'md5')

class VersionAdmin(admin.ModelAdmin):
    list_display = ('id', 'schema_version', 'app_version', 'user_data')

class VideoAdmin(admin.ModelAdmin):
    list_display = ('title', 'rating', 'video_name', 'directory_name', 'height', 'width', 'md5', 'clip_duration')
    list_filter = ('rating',)
    search_fields = ('md5', 'filename')

admin.site.register(BackingPhoto, BackingPhotoAdmin)
admin.site.register(Event, EventAdmin)
admin.site.register(Photo, PhotoAdmin)
admin.site.register(Tag, TagAdmin)
admin.site.register(Tombstone, TombstoneAdmin)
admin.site.register(Version, VersionAdmin)
admin.site.register(Video, VideoAdmin)
