# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from __future__ import unicode_literals

from django.db import models
import os


class BackingPhoto(models.Model):
    '''
    The BackingPhotoTable is designed to hold reference information to any
    number of alternative backing photos for a Photo object. Currently the only
    use is for the editable photo, which is generated when the user asks to
    edit a photo with an external program. (Shotwell's non-destructive edits
    are flattened to this editable backing file.)

    No transformations are held in the BackingPhotoTable. All transformations
    are held in the PhotoTable.
    '''
    id = models.IntegerField(primary_key=True)
    filepath = models.TextField(unique=True)
    timestamp = models.IntegerField(blank=True, null=True)
    filesize = models.IntegerField(blank=True, null=True)
    width = models.IntegerField(blank=True, null=True)
    height = models.IntegerField(blank=True, null=True)
    original_orientation = models.IntegerField(blank=True, null=True)
    file_format = models.IntegerField(blank=True, null=True)
    time_created = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'BackingPhotoTable'


class Event(models.Model):
    '''
    In Shotwell, an event is a grouping of photos based on the time of their
    exposure. A primary design note of events is that a photo can only belong
    to one event (or none at all). Thus, rather than each row in an EventTable
    maintaining it's list of photos, each photo in PhotoTable maintains an
    event_id. Thus, EventTable is a pretty simple table, maintaining only the
    event's name and it's primary photo (which is displayed as a thumbnail
    representing the entire event).
    '''
    id = models.IntegerField(primary_key=True)
    name = models.TextField(blank=True, null=True)
    primary_photo_id = models.IntegerField(blank=True, null=True)
    time_created = models.IntegerField(blank=True, null=True)
    primary_source_id = models.IntegerField(blank=True, null=True)
    comment = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'EventTable'


class Photo(models.Model):
    '''
    PhotoTable maintains information on all photos imported into the library.
    Most of its fields are self-explanatory. A couple of notes:

    - ImportID does not refer to a primary key of another table. Each round of
      imports is assigned a unique value for all photos in that import batch.
      The ImportID is, in fact, the time taken at the start of the import.
      Previously this was a conveniently unique number; now it is guaranteed,
      and so the date and ordering of all the import rolls can be relied upon
      by examining their IDs.

    - All transformations other than orientation (i.e. crop, color adjustment,
      etc.) are stored as a text KeyFile in the transformations column. This
      allows for new transformations to be easily added without modifying
      the database schema.
    - md5 is a full MD5 hash of the entire photo file. thumbnailmd5 is only
      of its embedded preview and exifmd5 is only of the EXIF data (not
      including the preview, which is commonly attached to the tail of the
      EXIF block).
    - backlinks are persistent links to container objects, i.e. Events and
      Tags. These are used when a Photo has been removed from one (for example,
      it's been trashed or marked offline). If the Photo is returned to the
      main heap of photos (i.e. untrashed), these backlinks are used to restore
      its connections to the containers.

    Because a low application start time is seen as valuable, optimization
    testing showed the quickest way to load the photos was to scoop up the
    entire PhotoTable in one transaction and then store the entire row in
    memory (one row per TransformablePhoto object). This is the most
    significant example of database caching in Shotwell.
    '''
    id = models.IntegerField(primary_key=True)
    filename = models.TextField(unique=True)
    width = models.IntegerField(blank=True, null=True)
    height = models.IntegerField(blank=True, null=True)
    filesize = models.IntegerField(blank=True, null=True)
    timestamp = models.IntegerField(blank=True, null=True)
    exposure_time = models.IntegerField(blank=True, null=True)
    orientation = models.IntegerField(blank=True, null=True)
    original_orientation = models.IntegerField(blank=True, null=True)
    import_id = models.IntegerField(blank=True, null=True)
    event = models.ForeignKey(Event, on_delete=models.SET_NULL, null=True)
    transformations = models.TextField(blank=True, null=True)
    md5 = models.TextField(blank=True, null=True)
    thumbnail_md5 = models.TextField(blank=True, null=True)
    exif_md5 = models.TextField(blank=True, null=True)
    time_created = models.IntegerField(blank=True, null=True)
    flags = models.IntegerField(blank=True, null=True)
    file_format = models.IntegerField(blank=True, null=True)
    title = models.TextField(blank=True, null=True)
    backlinks = models.TextField(blank=True, null=True)
    time_reimported = models.IntegerField(blank=True, null=True)
    editable_id = models.IntegerField(blank=True, null=True)
    rating = models.IntegerField(blank=True, null=True)
    metadata_dirty = models.IntegerField(blank=True, null=True)
    developer = models.TextField(blank=True, null=True)
    develop_shotwell_id = models.IntegerField(blank=True, null=True)
    develop_camera_id = models.IntegerField(blank=True, null=True)
    develop_embedded_id = models.IntegerField(blank=True, null=True)
    comment = models.TextField(blank=True, null=True)

    def image_name(self):
        '''
        Returns the name of the image file with the path
        '''
        return os.path.basename(self.filename)

    def directory_name(self):
        '''
        Returns the name of the parent directory_name
        '''
        return os.path.split(os.path.dirname(self.filename))[1]

    def thumbnail(self):
        '''
        Return the name of the thumbnail
        '''
        return "thumb{0:0=16x}.jpg".format(self.id)

    def tags(self):
        return Tag.objects.filter(photo_id_list__contains="thumb{0:0=16x}".format(self.id))

    def __unicode__(self):
        return self.image_name()

    class Meta:
        managed = False
        db_table = 'PhotoTable'


class Savedsearchdbtable(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.TextField(unique=True)
    operator = models.TextField()

    class Meta:
        managed = False
        db_table = 'SavedSearchDBTable'


class SavedsearchdbtableDate(models.Model):
    id = models.IntegerField(primary_key=True)
    search_id = models.IntegerField()
    search_type = models.TextField()
    context = models.TextField()
    date_one = models.TextField(blank=True, null=True)  # This field type is a guess.
    date_two = models.TextField(blank=True, null=True)  # This field type is a guess.

    class Meta:
        managed = False
        db_table = 'SavedSearchDBTable_Date'


class SavedsearchdbtableFlagged(models.Model):
    id = models.IntegerField(primary_key=True)
    search_id = models.IntegerField()
    search_type = models.TextField()
    flag_state = models.TextField()

    class Meta:
        managed = False
        db_table = 'SavedSearchDBTable_Flagged'


class SavedsearchdbtableMediatype(models.Model):
    id = models.IntegerField(primary_key=True)
    search_id = models.IntegerField()
    search_type = models.TextField()
    context = models.TextField()
    type = models.TextField(blank=True, null=True)  # This field type is a guess.

    class Meta:
        managed = False
        db_table = 'SavedSearchDBTable_MediaType'


class SavedsearchdbtableModified(models.Model):
    id = models.IntegerField(primary_key=True)
    search_id = models.IntegerField()
    search_type = models.TextField()
    context = models.TextField()
    modified_state = models.TextField()

    class Meta:
        managed = False
        db_table = 'SavedSearchDBTable_Modified'


class SavedsearchdbtableRating(models.Model):
    id = models.IntegerField(primary_key=True)
    search_id = models.IntegerField()
    search_type = models.TextField()
    rating = models.TextField(blank=True, null=True)  # This field type is a guess.
    context = models.TextField()

    class Meta:
        managed = False
        db_table = 'SavedSearchDBTable_Rating'


class SavedsearchdbtableText(models.Model):
    id = models.IntegerField(primary_key=True)
    search_id = models.IntegerField()
    search_type = models.TextField()
    context = models.TextField()
    text = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'SavedSearchDBTable_Text'


class Tag(models.Model):
    '''
    Photos may be tagged with a descriptive name and presented in groups. The
    TagTable holds a list of each tag and a serialized list of PhotoIDs that
    are tagged. Note this is different than Events, where each row in the
    PhotoTable holds an EventID; this is because of the different requirements
    for events and tags, where a photo can only be associated with a single
    event, but may be associated with many tags.
    '''
    id = models.IntegerField(primary_key=True)
    name = models.TextField(unique=True)
    photo_id_list = models.TextField(blank=True, null=True)
    time_created = models.IntegerField(blank=True, null=True)

    def thumbnail_list(self):
        return [photo + '.jpg' for photo in self.photo_id_list.split(',') if photo]


    def photo_list(self):
        photos = [int(thumbnail[5:].replace('.jpg', ''), 16) for thumbnail in self.thumbnail_list()]
        return Photo.objects.filter(pk__in=photos)

    def number_of_images(self):
        return len(self.thumbnail_list())

    def __unicode__(self):
        return self.name

    class Meta:
        managed = False
        db_table = 'TagTable'


class Tombstone(models.Model):
    '''
    The TombstoneTable is used in conjunction with library monitoring. If the
    user removes a photo from the library but does not delete the file, it's
    undesirable for Shotwell to notice the file at next startup and re-import
    it. A tombstone is literally that, a marker than the file is "dead".
    '''
    id = models.IntegerField(primary_key=True)
    filepath = models.TextField()
    filesize = models.IntegerField(blank=True, null=True)
    md5 = models.TextField(blank=True, null=True)
    time_created = models.IntegerField(blank=True, null=True)
    reason = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'TombstoneTable'


class Version(models.Model):
    id = models.IntegerField(primary_key=True)
    schema_version = models.IntegerField(blank=True, null=True)
    app_version = models.TextField(blank=True, null=True)
    user_data = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'VersionTable'


class Video(models.Model):
    '''
    The VideoTable is analogous to the PhotoTable. See PhotoTable for
    information on most of them. One column is different:

    - is_interpretable is set to true when Shotwell has detected that GStreamer
      (the multimedia streaming library Shotwell uses to generate video
      thumbnails) supports the video's format. In general, this means Shotwell
      was able to successfully generate a thumbnail for the video.
    '''
    id = models.IntegerField(primary_key=True)
    filename = models.TextField(unique=True)
    width = models.IntegerField(blank=True, null=True)
    height = models.IntegerField(blank=True, null=True)
    clip_duration = models.FloatField(blank=True, null=True)
    is_interpretable = models.IntegerField(blank=True, null=True)
    filesize = models.IntegerField(blank=True, null=True)
    timestamp = models.IntegerField(blank=True, null=True)
    exposure_time = models.IntegerField(blank=True, null=True)
    import_id = models.IntegerField(blank=True, null=True)
    event = models.ForeignKey(Event, on_delete=models.SET_NULL, null=True)
    md5 = models.TextField(blank=True, null=True)
    time_created = models.IntegerField(blank=True, null=True)
    rating = models.IntegerField(blank=True, null=True)
    title = models.TextField(blank=True, null=True)
    backlinks = models.TextField(blank=True, null=True)
    time_reimported = models.IntegerField(blank=True, null=True)
    flags = models.IntegerField(blank=True, null=True)
    comment = models.TextField(blank=True, null=True)

    def video_name(self):
        '''
        Returns the name of the video file with the path
        '''
        return os.path.basename(self.filename)

    def directory_name(self):
        '''
        Returns the name of the parent directory_name
        '''
        return os.path.split(os.path.dirname(self.filename))[1]

    class Meta:
        managed = False
        db_table = 'VideoTable'
