from django.shortcuts import get_object_or_404, render
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from .models import Photo, Tag

def render_photo_list(func):
    def wrapper(request, *args, **kw):
        photo_list = func(request, *args, **kw)

        paginator = Paginator(photo_list, 24, 6)

        page = request.GET.get('page')

        try:
            photos = paginator.page(page)
        except PageNotAnInteger:
            photos = paginator.page(1)
        except EmptyPage:
            photos = paginator.page(paginator.num_pages)

        context = {'photos': photos}

        return render(request, 'shotwell/list-photos.html', context)

    return wrapper


@render_photo_list
def entire_library(request):
    '''
    Return the entire photo library
    '''
    return Photo.objects.all().order_by('-id')


@render_photo_list
def tag(request, tag_name):
    '''
    Return thumbnails by tag
    '''
    tag = get_object_or_404(Tag, name=tag_name)

    return tag.photo_list()

@render_photo_list
def rating(request, rating):
    '''
    Return images with matching rating
    '''
    return Photo.objects.filter(rating=rating)


def list_tags(request):
    tag_list = Tag.objects.all()

    paginator = Paginator(tag_list, 24, 6)

    page = request.GET.get('page')

    try:
        tags = paginator.page(page)
    except PageNotAnInteger:
        tags = paginator.page(1)
    except EmptyPage:
        tags = paginator.page(paginator.num_pages)

    context = {'tags': tags}

    return render(request, 'shotwell/list_tags.html', context)
