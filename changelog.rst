Changelog
==========

v0.3.0
-------

- Add docstrings to models
- Add admin for BackingPhoto and Tombstone

v0.2.0
-------

- Improve the List view in the admin interface

v0.1.0
--------

- Django and its admin interface connect with Shotwell's photo.db
- Django uses separate database for other tables
